# README #

Estes documento README tem como objetivo fornecer as informações necessárias sobre o projeto Empresas.

### SOBRE O PROJETO ###

#### Bibliotecas utilizadas ####

* **Retrofit ->** Biblioteca para consumo de APIs e networking em geral, utilizei por ser uma das mais utilizadas.
* **androidx.navigation ->** Faz parte do Jetpack do android, gosto dessa biblioteca para Navegação por deixar tudo mais simples e prático.
* **ConstraintLayout ->** Ótima biblioteca para se trabalhar com a montagem de layouts responsivos, do mais simples ao mais complexo.
* **viewmodel ->** Utilizei para trabalhar com ViewModels na arquitetura escolhida.
* **Glide ->** Utilizei esta biblioteca por estar nas seção "Dicas", até o momento não havia trabalhada com cache de imagens.
* **Espresso ->** Biblioteca que estou mais familiarizado para implementação testes instrumentais.

#### Arquitetura e padrão de projeto ####

Trabalhei com o pattern **MVVM** e com **Clean Architecture**

#### Com mais tempo disponível eu teria... ####

* Implementado os testes instrumentais e unitários.
* Adicionados um segundo filtro "Por tipo da empresa", pois aparentemente a API estava preparada.

#### Como executar a aplicação? ####

1. Clone ou baixe este repositório.
2. No Android Studio, abra a pasta do projeto "empresas-android\application"
3. Execute a aplicação pelo Android Studio.
4. Para o acesso ao sistema utilize os seguintes dados:
	1. Login - teste_ioasys
	2. Senha - ioasys123