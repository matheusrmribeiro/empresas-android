package com.example.empresasandroid.src.app.pages.home

import android.app.Application
import android.content.Context
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.empresasandroid.R
import com.example.empresasandroid.src.app.pages.login.UILogin
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UILoginTest {

    private lateinit var scenario: FragmentScenario<UILogin>
    private lateinit var fragment: UILogin

    @Before
    fun setup() {
        scenario = launchFragmentInContainer<UILogin>()
        scenario.onFragment {
            fragment = it
        }
    }

    @Test
    fun test_initialize() {
        onView(withId(R.id.tvTitle)).check(matches(withText(fragment.resources.getString(R.string.login_title))))
        onView(withId(R.id.tvSubTitle)).check(matches(withText(fragment.resources.getString(R.string.login_sub_title))))
    }
}