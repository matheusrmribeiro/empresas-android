package com.example.empresasandroid.src.domain.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise (
    @SerializedName("id")
    val id: Long,
    @SerializedName("enterprise_name")
    val enterpriseName: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("email_enterprise")
    val emailEnterprise: String? = null,
    @SerializedName("facebook")
    val facebook: String? = null,
    @SerializedName("twitter")
    val twitter: String? = null,
    @SerializedName("linkedin")
    val linkedin: String? = null,
    @SerializedName("phone")
    val phone: String? = null,
    @SerializedName("own_enterprise")
    val ownEnterprise: Boolean,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("value")
    val value: Long,
    @SerializedName("shares")
    val shares: Long,
    @SerializedName("share_price")
    val sharePrice: Double,
    @SerializedName("own_shares")
    val ownShares: Long,
    @SerializedName("city")
    val city: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("enterprise_type")
    val enterpriseType: EnterpriseType
) : Parcelable

