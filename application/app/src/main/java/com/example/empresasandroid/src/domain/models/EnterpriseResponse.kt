package com.example.empresasandroid.src.domain.models

import com.google.gson.annotations.SerializedName

data class EnterpriseResponse(
    @SerializedName("enterprises")
    val enterprises: List<Enterprise>
) {}