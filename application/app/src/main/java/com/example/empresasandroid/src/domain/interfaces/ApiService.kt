package com.example.empresasandroid.src.domain.interfaces

import com.example.empresasandroid.src.data.Consts
import com.example.empresasandroid.src.domain.models.EnterpriseResponse
import com.example.empresasandroid.src.domain.models.LoginRequest
import com.example.empresasandroid.src.domain.models.LoginResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST(Consts.LOGIN_URL)
    fun login(@Body request: LoginRequest): Call<LoginResponse>

    @GET(Consts.ENTERPRISES)
    fun getEnterprises(@Query("name") name: String): Call<EnterpriseResponse>
}