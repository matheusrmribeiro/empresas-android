package com.example.empresasandroid.src.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.empresasandroid.R
import com.example.empresasandroid.src.data.Consts
import com.example.empresasandroid.src.domain.models.Enterprise
import kotlinx.android.synthetic.main.fragment_uihome_enterprise_item.view.*
import java.util.*

class EnterpriseAdapter(
    private val dataSet: List<Enterprise>,
    private val onClickCallback: (enterprise: Enterprise) -> Unit
) : RecyclerView.Adapter<EnterpriseAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {}

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_uihome_enterprise_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.itemView.tvTitle.text = item.enterpriseName
        viewHolder.itemView.tvSubTitle.text = item.enterpriseType.enterpriseTypeName
        val loc = Locale("", item.country)
        viewHolder.itemView.tvCountry.text = loc.displayCountry
        viewHolder.itemView.setOnClickListener {
            onClickCallback.invoke(item)
        }
        Glide.with(viewHolder.itemView.context)
            .load(Consts.IMAGE_URL + item.photo)
            .into(viewHolder.itemView.ivLogo)
    }


}