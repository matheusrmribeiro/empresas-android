package com.example.empresasandroid.src.app.pages.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.transition.TransitionManager
import com.example.empresasandroid.R
import com.example.empresasandroid.databinding.FragmentUiloginBinding
import com.example.empresasandroid.src.app.viewmodel.VMLogin
import com.example.empresasandroid.src.app.viewmodel.VMMain
import kotlinx.android.synthetic.main.fragment_uilogin.*

class UILogin : Fragment() {

    lateinit var binding: FragmentUiloginBinding
    lateinit var vmLogin: VMLogin
    lateinit var vmMain: VMMain

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_uilogin, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vmLogin = ViewModelProvider(requireActivity()).get(VMLogin::class.java)
        vmMain = ViewModelProvider(requireActivity()).get(VMMain::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = vmLogin
        vmLogin.context = requireContext()

        btLogin.setOnClickListener {
            vmLogin.login()
        }

        tiePassword.setOnEditorActionListener { it, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val inputManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(it.windowToken, 0)
                vmLogin.login()
                true
            } else {
                false
            }
        }

        ViewCompat.setTranslationZ(pbLoading, 10f)

        vmLogin.status.observe(viewLifecycleOwner, {
            if (it == VMLogin.Status.DONE)
                vmMain.setAuthenticated()
        })

        vmLogin.isLoading.observe(viewLifecycleOwner, {
            val constraint = ConstraintSet()
            if (it) {
                pbLoading.visibility = View.VISIBLE
                btLogin.text = ""
                btLogin.cornerRadius = resources.getDimensionPixelOffset(R.dimen.login_button_larger_corner)
                constraint.clone(uiLogin)
                constraint.constrainWidth(btLogin.id, resources.getDimensionPixelOffset(R.dimen.login_button_small_width))
            } else {
                pbLoading.visibility = View.GONE
                btLogin.text = resources.getString(R.string.login_login)
                btLogin.cornerRadius = resources.getDimensionPixelOffset(R.dimen.zero)
                constraint.clone(uiLogin)
                constraint.constrainWidth(btLogin.id, ConstraintSet.MATCH_CONSTRAINT_SPREAD)
            }
            TransitionManager.beginDelayedTransition(uiLogin)
            constraint.applyTo(uiLogin)
        })
    }

}