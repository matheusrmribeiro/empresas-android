package com.example.empresasandroid.src.app.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.empresasandroid.src.data.ApiClient
import com.example.empresasandroid.src.domain.models.Enterprise
import com.example.empresasandroid.src.domain.models.EnterpriseResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VMHome: ViewModel() {

    lateinit var context: Context
    val enterprises = MutableLiveData(listOf<Enterprise>())

    val withoutEnterprises: LiveData<Boolean> = Transformations.map(enterprises) {
        it.isEmpty() && (hasContent.value == true)
    }

    val hasContent = MutableLiveData(true)

    fun search(textQuery: String) {
        if (textQuery.isNotEmpty()) {
            val apiClient = ApiClient()
            apiClient.getApiService(context).getEnterprises(textQuery)
                .enqueue(object : Callback<EnterpriseResponse> {
                    override fun onFailure(call: Call<EnterpriseResponse>, t: Throwable) {
                        Log.e("SearchError", "erro")
                    }

                    override fun onResponse(
                        call: Call<EnterpriseResponse>,
                        response: Response<EnterpriseResponse>
                    ) {
                        if (response.code() == 200) {
                            enterprises.postValue(response.body()?.enterprises)
                            hasContent.value = response.body()?.enterprises?.isNotEmpty() ?: true
                        } else {
                            enterprises.postValue(listOf())
                            hasContent.value = false
                        }
                    }
                })
        } else {
            enterprises.postValue(listOf())
            hasContent.value = true
        }
    }

}