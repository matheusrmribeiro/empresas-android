package com.example.empresasandroid

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import com.example.empresasandroid.src.app.viewmodel.VMMain

class MainActivity : AppCompatActivity() {

    private lateinit var vmMain: VMMain

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vmMain = ViewModelProvider(this).get(VMMain::class.java)

        vmMain.authenticationStatus.observe(this, {
            val navController = findNavController(R.id.nav_host_fragment)
            when (it) {
                VMMain.AuthenticationStatus.AUTHENTICATED -> {
                    navController.navigate(R.id.UIHome)
                }
                VMMain.AuthenticationStatus.UNAUTHENTICATED -> {
                    navController.navigate(R.id.UILogin)
                }
            }
        })
    }

}