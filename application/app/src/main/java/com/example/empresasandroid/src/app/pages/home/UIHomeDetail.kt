package com.example.empresasandroid.src.app.pages.home

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.empresasandroid.R
import com.example.empresasandroid.src.data.Consts
import kotlinx.android.synthetic.main.fragment_uihome_detail.*


class UIHomeDetail : Fragment() {
    private val args: UIHomeDetailArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_uihome_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.title = args.enterprise.enterpriseName
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { //open navigation drawer when click navigation back button
            findNavController().navigateUp()
        }
        tvEnterpriseDescription.movementMethod = ScrollingMovementMethod()
        tvEnterpriseDescription.text = args.enterprise.description
        Glide.with(requireContext())
            .load(Consts.IMAGE_URL + args.enterprise.photo)
            .into(ivEnterpriseImage)
    }

}