package com.example.empresasandroid.src.data

object Consts {

    // Endpoints
    const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    const val IMAGE_URL = "https://empresas.ioasys.com.br"
    const val LOGIN_URL = "users/auth/sign_in"
    const val ENTERPRISES = "enterprises"

}