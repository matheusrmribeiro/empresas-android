package com.example.empresasandroid.src.app.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class VMMain : ViewModel() {

    enum class AuthenticationStatus { AUTHENTICATED, UNAUTHENTICATED }

    val authenticationStatus = MutableLiveData(AuthenticationStatus.UNAUTHENTICATED)

    fun setAuthenticated() {
        authenticationStatus.value = AuthenticationStatus.AUTHENTICATED
    }

    fun setUnauthenticated() {
        authenticationStatus.value = AuthenticationStatus.UNAUTHENTICATED
    }

}