package com.example.empresasandroid.src.app.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.ViewModel
import com.example.empresasandroid.src.data.ApiClient
import com.example.empresasandroid.src.data.SessionManager
import com.example.empresasandroid.src.domain.models.LoginRequest
import com.example.empresasandroid.src.domain.models.LoginResponse
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VMLogin: ViewModel() {

    enum class Status { IDLE, LOADING, DONE , ERROR}

    lateinit var context: Context
    val status = MutableLiveData(Status.IDLE)
    val isLoading: LiveData<Boolean> = map(status) {
        it == Status.LOADING
    }

    val email = MutableLiveData("")
    val password = MutableLiveData("")


    fun login() {
        setLOADING()
        val apiClient = ApiClient()
        val sessionManager = SessionManager(context)

        apiClient.getApiService(context, false).login(
            LoginRequest(
                email = email.value!!,
                password = password.value!!
            )
        )
            .enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    // Error logging in
                }

                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    val headers: Headers = response.headers()
                    val loginResponse = LoginResponse(
                        response.code(),
                        headers["uid"],
                        headers["access-token"],
                        headers["client"]
                    )

                    if (loginResponse.code == 200 && loginResponse.uid != null) {
                        sessionManager.saveSession(
                            loginResponse.authToken,
                            loginResponse.uid,
                            loginResponse.client
                        )
                        setDONE()
                    } else {
                        setERROR()
                    }
                }
            })
    }

    fun setIDLE() {
        status.value = Status.IDLE
    }

    fun setLOADING() {
        status.value = Status.LOADING
    }

    fun setDONE() {
        status.value = Status.DONE
    }

    fun setERROR() {
        status.value = Status.ERROR
    }

}