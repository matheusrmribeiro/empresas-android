package com.example.empresasandroid.src.app.pages.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.empresasandroid.R
import com.example.empresasandroid.databinding.FragmentUihomeBinding
import com.example.empresasandroid.src.app.adapters.EnterpriseAdapter
import com.example.empresasandroid.src.app.viewmodel.VMHome
import kotlinx.android.synthetic.main.fragment_uihome.*

class UIHome : Fragment() {

    lateinit var binding: FragmentUihomeBinding
    lateinit var vmHome: VMHome

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_uihome, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vmHome = ViewModelProvider(requireActivity()).get(VMHome::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = vmHome
        vmHome.context = requireContext()

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.mnSearch -> {
                }
            }
            true
        }


        val searchView = toolbar.menu.findItem(R.id.mnSearch).actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                vmHome.search(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

        vmHome.enterprises.observe(viewLifecycleOwner, {
            rvEnterprises.layoutManager = LinearLayoutManager(requireContext())
            rvEnterprises.adapter = EnterpriseAdapter(it) { enterprise ->
                val action = UIHomeDirections.actionUIHomeToUIHomeDetail(enterprise)
                findNavController().navigate(action)
            }
        })

    }

}